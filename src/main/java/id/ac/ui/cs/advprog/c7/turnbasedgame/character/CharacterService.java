package id.ac.ui.cs.advprog.c7.turnbasedgame.character;

import java.util.Collection;
import java.util.List;

public interface CharacterService {
    String attack(String user, String target);
    String skill(String user);
    Collection<Chara> getHeroes();
    Iterable<Chara> getEnemies();
    void resetStatsAndEnemy();
    String enemyAttack();
    String checkStatus();
    Chara getChara(String name);
    String levelUpHeroes();
}
