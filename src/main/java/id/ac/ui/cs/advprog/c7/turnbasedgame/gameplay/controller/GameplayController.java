package id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.controller;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;
import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core.GameSimulator;
import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.repository.GameRepository;
import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins="*")
@RequestMapping(path = "/v1/gameplay")
public class GameplayController {

    @Autowired
    private GameService gameService;

    @PostMapping(path = "/{token}", produces = {"application/json"})
    public ResponseEntity<String> createGameSimulator(@PathVariable(value = "token") String token) {
        String response = gameService.createGameSimulator(token);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping(path = "/{token}", produces = {"application/json"})
    public ResponseEntity<HttpStatus> deleteGameSimulatorByToken(@PathVariable(value = "token") String token){
        String response = gameService.deleteGameSimulator(token);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping(path = "/{token}/heroes", produces = {"application/json"})
    public ResponseEntity<Iterable<Chara>> getHeroesByToken(@PathVariable(value = "token") String token) {
        Iterable<Chara> heroes = gameService.getHeroes(token);
        return ResponseEntity.ok(heroes);
    }

    @GetMapping(path = "/{token}/enemies", produces = {"application/json"})
    public ResponseEntity<Iterable<Chara>> getEnemiesByToken(@PathVariable(value = "token") String token) {
        Iterable<Chara> enemies = gameService.getEnemies(token);
        return ResponseEntity.ok(enemies);
    }

    @GetMapping(path = "/{token}/{user}/attack/{target}", produces = {"application/json"})
    public ResponseEntity<List<String>> heroAttack(@PathVariable(value = "token") String token,
                                             @PathVariable(value= "user") String user,
                                             @PathVariable(value = "target") String target) {
        List<String> response = gameService.attack(token, user, target);
        return ResponseEntity.ok(response);
    }

    @GetMapping(path = "/{token}/{user}/skill", produces = {"application/json"})
    public ResponseEntity<List<String>> heroSkill(@PathVariable(value = "token") String token,
                                             @PathVariable(value= "user") String user) {
        List<String> response = gameService.skill(token, user);
        return ResponseEntity.ok(response);
    }

    @GetMapping(path = "/{token}/status", produces = {"application/json"})
    public ResponseEntity<String> getStateByToken(@PathVariable(value = "token") String token) {
        String state = gameService.getState(token);
        return ResponseEntity.ok(state);
    }

    @GetMapping(path = "/{token}/level-up", produces = {"application/json"})
    public ResponseEntity<String> levelUpHeroesByToken(@PathVariable(value = "token") String token) {
        String response = gameService.levelUpHeroes(token);
        return ResponseEntity.ok(response);
    }

//    @GetMapping(path = "/{token}/get-logs", produces = {"application/json"})
//    public ResponseEntity<List<String>> getAllLogsByToken(@PathVariable(value = "token") String token) {
//        List<String> response = gameService.getAllLogs(token);
//        return ResponseEntity.ok(response);
//    }
}
