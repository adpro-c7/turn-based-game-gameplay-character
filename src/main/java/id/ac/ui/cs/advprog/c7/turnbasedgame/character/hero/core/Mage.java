package id.ac.ui.cs.advprog.c7.turnbasedgame.character.hero.core;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.skill.ExplosionSkill;

public class Mage extends Chara {

    public Mage(int maxHealth, int maxSkillUsage, int attack) {
        super(maxHealth, maxSkillUsage, attack, "Mage");
        setSkill(new ExplosionSkill());
    }

    @Override
    public String attack() {
        return "mage attack";
    }
}
