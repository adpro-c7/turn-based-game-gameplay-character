package id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.CharacterServiceImpl;

public class GameFactoryImpl implements GameFactory{
    @Override
    public GameSimulator createGame() {
        return new GameSimulatorImpl(new CharacterServiceImpl());
    }
}
