package id.ac.ui.cs.advprog.c7.turnbasedgame.character.command.core;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;

public class MageSkillCommand extends SkillCommand{

    public MageSkillCommand(Chara user) {
        super(user, "enemy");
    }

    @Override
    public String execute() {
        if (user.getCurrentSkillUsage() > 0) {
            for (Chara target : targets) {
                target.attacked(user.getAttack() / 2);
            }
            user.attacked(user.getAttack() / 3);
        }
        return user.getName() + " use skill : " + user.skill();
    }

    @Override
    public String commandName() {
        return "Mage skill";
    }

    @Override
    public void setTarget(Chara target) {
        this.targets.add(target);
    }
}
