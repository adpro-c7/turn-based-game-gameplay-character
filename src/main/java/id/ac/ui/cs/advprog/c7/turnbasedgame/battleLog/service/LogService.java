package id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.service;

import id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.core.Log;
import java.util.ArrayList;
import java.util.List;

public interface LogService {
    void createLog(String token);
    List<Log> findAll();
    public Log findByToken(String token);
    public void save(String token, Log round);
    public void deleteByToken(String token);
}
