package id.ac.ui.cs.advprog.c7.turnbasedgame.character.skill;

public class ChargeAttackSkill implements Skill{
    @Override
    public String useSkill() {
        return "Damaged all enemies";
    }
}
