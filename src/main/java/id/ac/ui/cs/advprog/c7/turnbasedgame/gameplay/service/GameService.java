package id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.service;


import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;
import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core.GameSimulator;
import java.util.List;


public interface GameService {


    Iterable<Chara> getHeroes(String token);
    Iterable<Chara> getEnemies(String token);

    List<String> attack(String token, String user, String target);
    List<String> skill(String token, String user);

    String getState(String token);
    String deleteGameSimulator(String token);
    String createGameSimulator(String token);
    String levelUpHeroes(String token);
//    List<String> getAllLogs(String token);
//    Commented out on 5/18/2021 14:46
//    Iterable<String> getLogs(String token);

}
