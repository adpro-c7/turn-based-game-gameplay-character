package id.ac.ui.cs.advprog.c7.turnbasedgame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TurnBasedGameApplication {

	public static void main(String[] args) {
		SpringApplication.run(TurnBasedGameApplication.class, args);
	}

}
