package id.ac.ui.cs.advprog.c7.turnbasedgame.character.enemy.repository;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.enemy.core.Goblin;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.enemy.core.Ogre;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.enemy.core.Troll;

import java.util.HashMap;
import java.util.Random;

public class EnemyRepository {
    private HashMap<String, Chara> enemies;
    private int levelEnemy;

    public EnemyRepository() {
        newEnemies();
        levelEnemy = 0;
    }

    public void registerEnemy(Chara enemy) {
        enemies.put(enemy.getName(), enemy);
    }

    public Chara getEnemy(String name) {
        return enemies.get(name);
    }

    public Iterable<Chara> getEnemies() {
        return enemies.values();
    }

    public int getLevelEnemy() { return this.levelEnemy; }

    public boolean checkStatus() {
        for (Chara enemy : enemies.values()) {
            if (enemy.isAlive()) return true;
        }
        return false;
    }

    public void newEnemies() {
        enemies = new HashMap<>();
        int maxHealth;
        int attack;

        maxHealth = createNumberInRange(100,90);
        maxHealth += ((maxHealth * 0.2) * levelEnemy);
        attack = createNumberInRange(40,35);
        attack += ((attack * 0.2) * levelEnemy);
        Chara goblin = new Goblin(maxHealth,0, attack);

        maxHealth = createNumberInRange(370,350);
        maxHealth += ((maxHealth * 0.2) * levelEnemy);
        attack = createNumberInRange(27,20);
        attack += ((attack * 0.2) * levelEnemy);
        Chara ogre = new Ogre(maxHealth, 0, attack);

        maxHealth = createNumberInRange(135,115);
        maxHealth += ((maxHealth * 0.2) * levelEnemy);
        attack = createNumberInRange(75,55);
        attack += ((attack * 0.2) * levelEnemy);
        Chara troll = new Troll(maxHealth, 0, attack);

        enemies.put(goblin.getName(), goblin);
        enemies.put(ogre.getName(), ogre);
        enemies.put(troll.getName(), troll);
    }

    public void balanceEnemies() {
        this.levelEnemy = this.levelEnemy + 1;
    }

    private int createNumberInRange(int max, int min) {
        Random random = new Random();

        return random.nextInt((max-min) + 1) + min;
    }

}
