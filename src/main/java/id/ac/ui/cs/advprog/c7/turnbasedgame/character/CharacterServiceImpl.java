package id.ac.ui.cs.advprog.c7.turnbasedgame.character;

import id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.service.LogService;
import id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.service.LogServiceImpl;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.command.service.CommandService;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.command.service.CommandServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CharacterServiceImpl implements CharacterService {


    CommandService commandService;

    CharacterRepository characterRepository;

    LogService logService;

    private Random random;

    public CharacterServiceImpl() {
        characterRepository = new CharacterRepository();
        commandService = new CommandServiceImpl(characterRepository);
        logService = new LogServiceImpl();
        random = new Random();
    }

    @Override
    public String attack(String user, String target) {
        Chara targetChara = characterRepository.getCharacter(target);
        return commandService.attack(user , targetChara);
    }

    @Override
    public String skill(String user) {
        return commandService.skill(user);
    }

    @Override
    public Collection<Chara> getHeroes() {
        return characterRepository.getHeroes();
    }

    @Override
    public Iterable<Chara> getEnemies() {
        return characterRepository.getEnemies();
    }

    @Override
    public void resetStatsAndEnemy() {
        characterRepository.resetStatsAndEnemy();
        commandService.resetTargets();
    }

    @Override
    public String enemyAttack() {
        String enemy = randomEnemy().getName();
        String hero = randomHero().getName();

        return this.attack(enemy, hero);
    }

    @Override
    public String checkStatus() {
        return characterRepository.checkStatus();
    }

    @Override
    public Chara getChara(String name) {
        return characterRepository.getCharacter(name);
    }

    private Chara randomEnemy() {
        ArrayList<Chara> enemies = new ArrayList<>();
        int selectEnemy = 0;

        for(Chara enemy : characterRepository.getEnemies()) {
            enemies.add(enemy);
        }

        do {
            selectEnemy = Math.abs(random.nextInt()%3);
        } while (!enemies.get(selectEnemy).isAlive());

        return enemies.get(selectEnemy);
    }

    private Chara randomHero() {
        ArrayList<Chara> heroes = new ArrayList<>();
        int selectHero = 0;

        for(Chara hero : characterRepository.getHeroes()) {
            heroes.add(hero);
        }

        do {
            selectHero = Math.abs(random.nextInt()%3);
        } while (!heroes.get(selectHero).isAlive());

        return heroes.get(selectHero);
    }

    @Override
    public String levelUpHeroes() {
        characterRepository.levelUpHeroes();
        return "All heroes have leveled up!";
    }
}
