package id.ac.ui.cs.advprog.c7.turnbasedgame.character;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.hero.core.Healer;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.hero.core.Knight;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.hero.core.Mage;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.hero.repository.HeroRepository;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.enemy.repository.EnemyRepository;

import java.util.Collection;
import java.util.List;


public class CharacterRepository {

    HeroRepository heroRepository;
    EnemyRepository enemyRepository;

    public CharacterRepository() {
        this.heroRepository = new HeroRepository();
        this.enemyRepository = new EnemyRepository();

        Chara healer = new Healer(150, 3, 20);
        Chara knight = new Knight(300, 4, 30);
        Chara mage = new Mage(115, 5, 50);

        heroRepository.regiserHero(healer);
        heroRepository.regiserHero(knight);
        heroRepository.regiserHero(mage);

        enemyRepository.newEnemies();
    }

    public Chara getHero(String name) {
        return heroRepository.getHero(name);
    }

    public Chara getEnemy(String name) {
        return enemyRepository.getEnemy(name);
    }

    public Chara getCharacter(String name) {
        Chara chara = getHero(name);
        if(chara == null) {
            chara = getEnemy(name);
        }
        return chara;
    }

    public Collection<Chara> getHeroes() {
        return heroRepository.getHeroes();
    }

    public Iterable<Chara> getEnemies() {
        return enemyRepository.getEnemies();
    }

    public void resetStatsAndEnemy() {
        heroRepository.resetStats();
        enemyRepository.newEnemies();
    }

    public String checkStatus() {
        boolean statusHero = heroRepository.checkStatus();
        boolean statusEnemy = enemyRepository.checkStatus();

        if (!statusHero) {
            return "LOSE";
        } else if (!statusEnemy) {
            return "WIN";
        } else {
            return "CONTINUE";
        }
    }

    public void levelUpHeroes() {
        heroRepository.levelUpHeroes();
        enemyRepository.balanceEnemies();
    }
}
