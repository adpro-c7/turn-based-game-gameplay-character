package id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.service;

import id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.core.Log;
import id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.service.LogService;
import id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.service.LogServiceImpl;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;
import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core.GameFactory;
import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core.GameSimulator;
import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GameServiceImpl implements GameService{

    private GameRepository gameRepository;

    private LogService logService;

    @Autowired
    public GameServiceImpl(GameRepository gameRepository, LogServiceImpl logService) {
        this.gameRepository = gameRepository;
        this.logService = logService;
    }


    @Override
    public Iterable<Chara> getHeroes(String token) {
        GameSimulator gameSimulator = gameRepository.getGameSimulatorByToken(token);
        return gameSimulator.getHeroes();

    }

    @Override
    public Iterable<Chara> getEnemies(String token) {
        GameSimulator gameSimulator = gameRepository.getGameSimulatorByToken(token);
        return gameSimulator.getEnemies();
    }

    @Override
    public List<String> attack(String token, String user, String target) {
        GameSimulator gameSimulator = gameRepository.getGameSimulatorByToken(token);
        List<String> response = gameSimulator.attack(user, target);
        Log log = logService.findByToken(token);
        log.addLogs(response);
        return response;
    }

    @Override
    public List<String> skill(String token, String user) {
        GameSimulator gameSimulator = gameRepository.getGameSimulatorByToken(token);
        List<String> response = gameSimulator.skill(user);
        Log log = logService.findByToken(token);
        log.addLogs(response);
        return response;
    }

    @Override
    public String getState(String token) {
        GameSimulator gameSimulator = gameRepository.getGameSimulatorByToken(token);
        return gameSimulator.getState().toString();
    }

    @Override
    public String deleteGameSimulator(String token) {
        return gameRepository.deleteGameSimulatorByToken(token);
    }

    @Override
    public String createGameSimulator(String token) {
        GameFactory gameFactory = gameRepository.getGameFactory();
        GameSimulator gameSimulator = gameFactory.createGame();
        gameRepository.registerGameSimulatorByToken(token, gameSimulator);
        logService.createLog(token);
        return "Game has been created.";
    }

    @Override
    public String levelUpHeroes(String token) {
        GameSimulator gameSimulator = gameRepository.getGameSimulatorByToken(token);
        String response = gameSimulator.levelUpHeroes();
        Log log = logService.findByToken(token);
        log.addLog(response);
        return response;
    }

//    @Override
//    public List<String> getAllLogs(String token) {
//        GameSimulator gameSimulator = gameRepository.getGameSimulatorByToken(token);
//        return gameSimulator.getAllLogs();
//    }

//    Commented out on 5/18/2021 14:46
//    @Override
//    public Iterable<String> getLogs(String token) {
//        return null;
//    }
//
}
