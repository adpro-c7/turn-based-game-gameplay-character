package id.ac.ui.cs.advprog.c7.turnbasedgame.character.command.service;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;

import java.util.List;

public interface CommandService {
    String attack(String character, Chara target);
    String skill(String character);
    void resetTargets();
}