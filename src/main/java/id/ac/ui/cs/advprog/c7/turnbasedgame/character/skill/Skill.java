package id.ac.ui.cs.advprog.c7.turnbasedgame.character.skill;

public interface Skill {
    String useSkill();
}
