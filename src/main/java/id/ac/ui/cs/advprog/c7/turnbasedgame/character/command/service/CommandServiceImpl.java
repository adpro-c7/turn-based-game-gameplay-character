package id.ac.ui.cs.advprog.c7.turnbasedgame.character.command.service;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.CharacterRepository;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.command.core.*;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.command.repository.CommandRepository;

public class CommandServiceImpl implements CommandService{
    CommandRepository commandRepository;
    CharacterRepository characterRepository;

    public CommandServiceImpl(CharacterRepository characterRepository1) {
        this.commandRepository = new CommandRepository();
        this.characterRepository = characterRepository1;

        AttackCommand healerAttack = new HealerAttackCommand(characterRepository.getCharacter("Healer"));
        AttackCommand knightAttack = new KnightAttackCommand(characterRepository.getCharacter("Knight"));
        AttackCommand mageAttack = new MageAttackCommand(characterRepository.getCharacter("Mage"));
        AttackCommand goblinAttack = new GoblinAttackCommand(characterRepository.getCharacter("Goblin"));
        AttackCommand ogreAttack = new OgreAttackCommand(characterRepository.getCharacter("Ogre"));
        AttackCommand trollAttack = new TrollAttackCommand(characterRepository.getCharacter("Troll"));

        SkillCommand healerSkill = new HealerSkillCommand(characterRepository.getCharacter("Healer"));
        SkillCommand knightSkill = new KnightSkillCommand(characterRepository.getCharacter("Knight"));
        SkillCommand mageSkill = new MageSkillCommand(characterRepository.getCharacter("Mage"));

        commandRepository.registerAttackCommand(healerAttack);
        commandRepository.registerAttackCommand(knightAttack);
        commandRepository.registerAttackCommand(mageAttack);
        commandRepository.registerAttackCommand(goblinAttack);
        commandRepository.registerAttackCommand(ogreAttack);
        commandRepository.registerAttackCommand(trollAttack);

        commandRepository.registerSkillCommand(healerSkill, characterRepository.getHeroes());
        commandRepository.registerSkillCommand(knightSkill, characterRepository.getEnemies());
        commandRepository.registerSkillCommand(mageSkill, characterRepository.getEnemies());
    }

    @Override
    public String attack(String character, Chara target) {
        return commandRepository.executeAttackCommand(character + " attack", target);
    }

    @Override
    public String skill(String character) {
        return commandRepository.executeSkillCommand(character + " skill");
    }

    @Override
    public void resetTargets() {
        commandRepository.resetCommandTarget(characterRepository.getHeroes(), characterRepository.getEnemies());
    }
}