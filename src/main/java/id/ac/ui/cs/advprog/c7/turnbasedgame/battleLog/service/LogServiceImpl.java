package id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.service;

import id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.core.Log;
import id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.repository.LogRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LogServiceImpl implements LogService{


    private LogRepository logRepository;
    private String allLog;

    public LogServiceImpl() {
        logRepository = new LogRepository();
    }

    @Override
    public void createLog(String token){
        Log log = findByToken(token);
        if (log == null){
            log = new Log();
        }
        save(token, log);
    }

    @Override
    public Log findByToken(String token){
        return logRepository.findByToken(token);
    }

    @Override
    public List<Log> findAll() {
        return logRepository.findAll();
    }

    @Override
    public void save(String token, Log log){
        logRepository.save(token, log);
    }

    @Override
    public void deleteByToken(String token){
        logRepository.deleteByToken(token);
    }
}
