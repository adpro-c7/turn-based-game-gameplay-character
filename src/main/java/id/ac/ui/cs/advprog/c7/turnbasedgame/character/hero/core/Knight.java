package id.ac.ui.cs.advprog.c7.turnbasedgame.character.hero.core;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.skill.ChargeAttackSkill;

public class Knight extends Chara {

    public Knight(int maxHealth, int maxSkillUsage, int attack) {
        super(maxHealth, maxSkillUsage, attack, "Knight");
        setSkill(new ChargeAttackSkill());
    }

    @Override
    public String attack() {
        return "knight attack";
    }
}
