package id.ac.ui.cs.advprog.c7.turnbasedgame.character.command.core;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;

public class MageAttackCommand extends AttackCommand{

    public MageAttackCommand(Chara user) {
        super(user);
    }

    @Override
    public String execute() {
        target.attacked(user.getAttack());
        return user.getName() + " attacked " + target.getName() + " : " + user.attack();
    }

    @Override
    public String commandName() {
        return "Mage attack";
    }
}
