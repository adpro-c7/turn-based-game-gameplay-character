package id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.core;

import java.util.ArrayList;
import java.util.List;

public class Log {

    private List<String> logs;

    public Log(){
        this.logs = new ArrayList<>();
    }

    public void addLog(String newLog) {
        this.logs.add(newLog);
    }

    public void addLogs(List<String> newLogs) {
        this.logs.addAll(newLogs);
    }

    public List<String> reset() {
        this.logs = new ArrayList<>();
        return this.logs;
    }

    public List<String> getCurrentLog(){
        return this.logs;
    }
}
