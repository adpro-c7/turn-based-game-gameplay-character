package id.ac.ui.cs.advprog.c7.turnbasedgame.character.enemy.core;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.skill.HaveNoSkill;

public class Goblin extends Chara {

    public Goblin(int maxHealth, int maxSkillUsage, int attack) {
        super(maxHealth, maxSkillUsage, attack, "Goblin");
        this.setSkill(new HaveNoSkill());
    }

    @Override
    public String attack() {
        return "goblin attack";
    }
}
