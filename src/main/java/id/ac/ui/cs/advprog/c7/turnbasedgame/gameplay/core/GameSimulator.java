package id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;
import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core.GameState;

import java.util.List;

public interface GameSimulator {

    List<String> attack(String user, String target);
    List<String> skill(String user);
    GameState getState();
    Iterable<Chara> getHeroes();
    Iterable<Chara> getEnemies();
    void setState(GameState gameState);
    String levelUpHeroes();
//    List<String> getAllLogs();
}
