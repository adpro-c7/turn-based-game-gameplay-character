package id.ac.ui.cs.advprog.c7.turnbasedgame.character.skill;

public class ExplosionSkill implements Skill{
    @Override
    public String useSkill() {
        return "EXPLOSIOON";
    }
}
