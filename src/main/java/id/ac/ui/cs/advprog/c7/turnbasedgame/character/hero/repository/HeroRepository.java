package id.ac.ui.cs.advprog.c7.turnbasedgame.character.hero.repository;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.hero.core.Mage;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class HeroRepository {
    private HashMap<String, Chara> heroes;

    public HeroRepository() {
        heroes = new HashMap<>();
    }

    public void regiserHero(Chara hero) {
        heroes.put(hero.getName(), hero);
    }

    public Chara getHero(String name) {
        return heroes.get(name);
    }

    public Collection<Chara> getHeroes() {
        return heroes.values();
    }

    public void resetStats() {
        for (Chara chara: heroes.values()) {
            chara.resetStats();
        }
    }

    public boolean checkStatus() {
        boolean status = false;
        for (Chara chara: heroes.values()) {
            if (chara.isAlive()) {
                status = true;
                break;
            }
        }
        return status;
    }

    public void levelUpHeroes() {
        for (Chara chara: heroes.values()) {
            chara.levelUp();
        }
    }
}
