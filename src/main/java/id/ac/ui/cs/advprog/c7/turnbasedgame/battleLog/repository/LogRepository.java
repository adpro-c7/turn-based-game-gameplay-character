package id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.repository;

import org.springframework.stereotype.Repository;
import id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.core.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class LogRepository {

    private Map<String, Log> logMap = new HashMap<>();

    public LogRepository(){}

    public List<Log> findAll() {
        return new ArrayList(logMap.values());
    }

    public void save(String token, Log log) {
        logMap.put(token, log);
    }

    public Log findByToken(String token) {
        return logMap.get(token);
    }

    public void deleteByToken(String token){
        logMap.remove(token);
    }
}


