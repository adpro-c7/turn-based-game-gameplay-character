package id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.CharacterService;

import java.util.ArrayList;
import java.util.List;

public class GameSimulatorImpl implements GameSimulator {
    CharacterService characterService;
    GameState gameState;

    public GameSimulatorImpl(CharacterService characterService) {
        this.characterService = characterService;
        this.gameState = GameState.PLAYER_TURN;
    }

    @Override
    public List<String> attack(String user, String target) {
        List<String> responses = new ArrayList<>();
        if(gameState == GameState.PLAYER_TURN) {
            responses.add(characterService.attack(user, target));
            checkAndSetStatus();
            if(gameState == GameState.PLAYER_TURN) {
                responses.add(characterService.enemyAttack());
                checkAndSetStatus();
            }
        }

        return responses;
    }

    @Override
    public List<String> skill(String user) {
        List<String> responses = new ArrayList<>();
        if(gameState == GameState.PLAYER_TURN) {
            responses.add(characterService.skill(user));
            checkAndSetStatus();
            if(gameState == GameState.PLAYER_TURN) {
                responses.add(characterService.enemyAttack());
                checkAndSetStatus();
            }
        }

        return responses;
    }

    @Override
    public GameState getState() {
        GameState gameStateNow = gameState;
        if (gameStateNow == GameState.WIN || gameStateNow == GameState.LOSE) {
            setState(GameState.PLAYER_TURN);
            characterService.resetStatsAndEnemy();
        }
        return gameStateNow;
    }

    @Override
    public Iterable<Chara> getHeroes() {
        return characterService.getHeroes();
    }

    @Override
    public Iterable<Chara> getEnemies() {
        return characterService.getEnemies();
    }

    @Override
    public void setState(GameState gameState) {
        this.gameState = gameState;
    }

    @Override
    public String levelUpHeroes() {
        return characterService.levelUpHeroes();
    }

//    @Override
//    public List<String> getAllLogs() {
//        return characterService.getAllLogs();
//    }

    private void checkAndSetStatus() {
        String gameStatus = characterService.checkStatus();
        if(gameStatus.equals("WIN")) {
            setState(GameState.WIN);
        } else if(gameStatus.equals("LOSE")) {
            setState(GameState.LOSE);
        }
    }

}
