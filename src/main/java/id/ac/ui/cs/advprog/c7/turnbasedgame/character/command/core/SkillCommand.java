package id.ac.ui.cs.advprog.c7.turnbasedgame.character.command.core;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class SkillCommand implements Command {
    protected Chara user;
    protected String targetType;
    protected List<Chara> targets;

    public SkillCommand(Chara user, String targetType) {
        this.user = user;
        this.targets = new ArrayList<>();
        this.targetType = targetType;
    }

    public String getTargetType() {
        return this.targetType;
    }

    public void replaceTargets(Iterable<Chara> newTargets) {
        targets.clear();
        for (Chara target: newTargets) {
            setTarget(target);
        }
    }
}
