package id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core;

public enum GameState {
    PLAYER_TURN,
    WIN,
    LOSE
}
