package id.ac.ui.cs.advprog.c7.turnbasedgame.character.command.core;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;

public abstract class AttackCommand implements Command{
    protected Chara user;
    protected Chara target;

    public AttackCommand(Chara user) {
        this.user = user;
    }
    
    @Override
    public void setTarget(Chara target) {
        this.target = target;
    }
}