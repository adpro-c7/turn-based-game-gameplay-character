package id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.repository;

import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core.GameFactory;
import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core.GameFactoryImpl;
import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core.GameSimulator;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class GameRepositoryImpl implements GameRepository{

    private Map<String, GameSimulator> gameMap;
    private GameFactory gameFactory;

    public GameRepositoryImpl() {
        gameMap = new HashMap<>();
        gameFactory = new GameFactoryImpl();
    }

    @Override
    public GameSimulator getGameSimulatorByToken(String token) {
        return gameMap.get(token);
    }

    @Override
    public String deleteGameSimulatorByToken(String token) {
        gameMap.remove(token);
        return null;
    }

    @Override
    public GameSimulator registerGameSimulatorByToken(String token, GameSimulator gameSimulator) {
        gameMap.put(token, gameSimulator);
        return gameSimulator;
    }

    @Override
    public GameFactory getGameFactory() {
        return gameFactory;
    }
}
