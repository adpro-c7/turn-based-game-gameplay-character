package id.ac.ui.cs.advprog.c7.turnbasedgame.character;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.skill.Skill;

public abstract class Chara {
    protected String name;
    protected int maxHealth;
    protected int currentHealth;
    protected int attack;
    protected int maxSkillUsage;
    protected int currentSkillUsage;
    protected boolean isAlive;
    protected Skill skill;

    public Chara(int maxHealth, int maxSkillUsage, int attack, String name) {
        this.maxHealth = maxHealth;
        this.maxSkillUsage = maxSkillUsage;
        this.attack = attack;
        this.name = name;
        resetStats();
    }

    public String getName() {
        return name;
    }

    public int getAttack() {
        return this.attack;
    }

    public int getMaxHealth() {
        return this.maxHealth;
    }

    public int getCurrentHealth() {
        return this.currentHealth;
    }

    public int getMaxSkillUsage() {
        return this.maxSkillUsage;
    }

    public int getCurrentSkillUsage() {
        return this.currentSkillUsage;
    }

    public boolean isAlive() {
        return this.isAlive;
    }

    protected void setIsAlive(boolean val) {
        this.isAlive = val;
    }

    protected void setSkill(Skill skill) {
        this.skill = skill;
    }

    public void attacked(int attackPoint) {
        this.currentHealth -= attackPoint;
        validHealth();
    }

    public void heal(int healPoint) {
        this.currentHealth += healPoint;
        validHealth();
    }

    private void validHealth() {
        if(currentHealth <= 0) {
            this.currentHealth = 0;
            setIsAlive(false);
        } else if (currentHealth > maxHealth) {
            this.currentHealth = maxHealth;
        }
    }

    public void resetStats() {
        this.currentHealth = maxHealth;
        this.currentSkillUsage = maxSkillUsage;
        setIsAlive(true);
    }

    public String skill() {
        if(currentSkillUsage == 0) {
            return "Cannot use skill!";
        } else {
            currentSkillUsage--;
            return this.skill.useSkill();
        }
    }

    public void levelUp() {
        this.maxHealth += maxHealth * 0.2;
        this.attack += attack * 0.2;
        resetStats();
    }

    public abstract String attack();
}
