package id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.service;

import id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.core.Log;
import id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.repository.LogRepository;
import id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.service.LogService;
import id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.service.LogServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class LogServiceTest {

    private Class<?> logServiceClass;

    @Mock
    LogRepository logRepository;

    LogService logService;

    @BeforeEach
    public void setup() throws Exception {
        logServiceClass = Class.forName(
                "id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.service.LogServiceImpl");
        logRepository = new LogRepository();
        logService = new LogServiceImpl();
        Log logTest = new Log();
        logRepository.save("tokenTest", logTest);
    }

    @Test
    public void testLogServiceHasCreateLogMethod() throws Exception {
        ReflectionTestUtils.setField(logService, "logRepository", logRepository);
        logService.createLog("token");
        Log log = logService.findByToken("token");
        assertThat(log).isEqualTo(logRepository.findByToken("token"));
    }

    @Test
    public void testLogServiceHasFindByTokenMethod() throws Exception {
        ReflectionTestUtils.setField(logService, "logRepository", logRepository);
        Log acquiredLog = logService.findByToken("tokenTest");

        assertThat(acquiredLog).isEqualTo(logRepository.findByToken("tokenTest"));
    }

    @Test
    public void testLogServiceHasFindAllMethod() throws Exception {
        ReflectionTestUtils.setField(logService, "logRepository", logRepository);
        List<Log> acquiredLog = logService.findAll();

        assertThat(acquiredLog).isEqualTo(logRepository.findAll());
    }

    @Test
    public void testLogServiceHasSaveMethod() throws Exception {
        ReflectionTestUtils.setField(logService, "logRepository", logRepository);
        Log log = new Log();
        logService.save("token", log);
        Log acquiredLog = logService.findByToken("token");

        assertThat(acquiredLog).isEqualTo(logRepository.findByToken("token"));
    }

    @Test
    public void testLogServiceHasDeleteByTokenMethod() throws Exception {
        ReflectionTestUtils.setField(logService, "logRepository", logRepository);
        Log log = new Log();
        logService.save("token", log);
        logService.deleteByToken("token");
        Log acquiredLog = logService.findByToken("token");

        assertThat(acquiredLog).isEqualTo(null);
    }
}
