package id.ac.ui.cs.advprog.c7.turnbasedgame.character.hero.repository;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.hero.core.Healer;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.hero.core.Knight;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.hero.core.Mage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.*;

public class HeroRepositoryTest {

    private HeroRepository heroRepository;

    @BeforeEach
    public void setUp() {
        heroRepository = new HeroRepository();
        Chara healer = new Healer(150, 3, 20);
        Chara knight = new Knight(300, 4, 30);
        Chara mage = new Mage(115, 5, 50);

        heroRepository.regiserHero(healer);
        heroRepository.regiserHero(knight);
        heroRepository.regiserHero(mage);
    }

    @Test
    public void testGetHeroShouldReturnCorrectly() throws Exception {
        Chara knight = heroRepository.getHero("Knight");
        assertEquals(Knight.class, knight.getClass());
    }

    @Test
    public void testGetHeroesShouldReturnListOfHeroes() throws Exception {
        Collection<Chara> heroes = heroRepository.getHeroes();
        assertThat(heroes).isNotEmpty();
    }

    @Test
    public void testLevelUpHeroesShouldIncreaseStats() throws Exception {
        int maxHealthKnight = heroRepository.getHero("Knight").getMaxHealth();
        int maxHealthHealer = heroRepository.getHero("Healer").getMaxHealth();
        int maxHealthMage = heroRepository.getHero("Mage").getMaxHealth();

        heroRepository.levelUpHeroes();

        assertTrue(maxHealthKnight < heroRepository.getHero("Knight").getMaxHealth());
        assertTrue(maxHealthHealer < heroRepository.getHero("Healer").getMaxHealth());
        assertTrue(maxHealthMage < heroRepository.getHero("Mage").getMaxHealth());
    }
}
