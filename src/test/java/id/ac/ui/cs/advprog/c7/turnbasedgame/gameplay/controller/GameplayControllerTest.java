package id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.service.LogService;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;
import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.service.GameService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
public class GameplayControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private GameService gameService;

    @MockBean
    private LogService logService;


    private String token;

    private String response;

    private String baseUrl;

    private Chara chara;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        token = "mockToken";
        response = "response";
        baseUrl = "/v1/gameplay";
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testControllerCreateGame() throws Exception {
        String url = String.format("%s/%s", baseUrl, token);
        when(gameService.createGameSimulator(token)).thenReturn(response);
        mvc.perform(post(url).contentType(MediaType.APPLICATION_JSON).content(mapToJson(response)));
    }

    @Test
    public void testControllerDeleteGame() throws Exception {
        String url = String.format("%s/%s", baseUrl, token);
        when(gameService.deleteGameSimulator(token)).thenReturn(response);
        mvc.perform(delete(url).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent());
    }

    @Test
    public void testControllerLevelUpHeroes() throws Exception {
        String url = String.format("%s/%s/level-up", baseUrl, token);
        when(gameService.levelUpHeroes(token)).thenReturn(response);
        mvc.perform(get(url).contentType(MediaType.APPLICATION_JSON).content(mapToJson(response)));
    }

    @Test
    public void testControllerGetState() throws Exception {
        String url = String.format("%s/%s/status", baseUrl, token);
        when(gameService.getState(token)).thenReturn(response);
        mvc.perform(get(url).contentType(MediaType.APPLICATION_JSON).content(mapToJson(response)));
    }

    @Test
    public void testControllerGetEnemies() throws Exception {
        String url = String.format("%s/%s/enemies", baseUrl, token);
        Iterable<Chara> enemies = Arrays.asList(chara);
        when(gameService.getEnemies(token)).thenReturn(enemies);
        mvc.perform(get(url).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    public void testControllerGetHeroes() throws Exception {
        String url = String.format("%s/%s/heroes", baseUrl, token);
        Iterable<Chara> heroes = Arrays.asList(chara);
        when(gameService.getEnemies(token)).thenReturn(heroes);
        mvc.perform(get(url).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    public void testControllerHeroAttack() throws Exception {
        String user = "mockUser";
        String target = "mockTarget";
        String url = String.format("%s/%s/%s/attack/%s", baseUrl, token, user, target);
        List<String> responses = Arrays.asList(response);
        when(gameService.attack(token, user, target)).thenReturn(responses);
        mvc.perform(get(url).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    public void testControllerHeroSkill() throws Exception {
        String user = "mockUser";
        String url = String.format("%s/%s/%s/skill", baseUrl, token, user);
        List<String> responses = Arrays.asList(response);
        when(gameService.skill(token, user)).thenReturn(responses);
        mvc.perform(get(url).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

}
