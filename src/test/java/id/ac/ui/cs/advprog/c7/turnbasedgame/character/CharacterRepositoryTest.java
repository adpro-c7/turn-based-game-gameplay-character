package id.ac.ui.cs.advprog.c7.turnbasedgame.character;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.hero.core.Healer;
import org.assertj.core.internal.Iterables;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class CharacterRepositoryTest {
    private Class<?> characterRepositoryClass;

    @Mock
    CharacterRepository characterRepository;

    @BeforeEach
    public void setup() throws Exception {
        characterRepositoryClass = Class.forName("id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara");
        characterRepository = new CharacterRepository();
    }

    @Test
    public void testCharacterRepositoryGetHeroShouldReturnCorrectly() throws Exception {
        Chara result = characterRepository.getHero("Healer");
        assertEquals(Healer.class, result.getClass());
    }

    @Test
    public void testCharacterRepositoryGetCharacterShouldReturnCorrectly() throws Exception {
        Chara result = characterRepository.getHero("Healer");
        assertEquals(Healer.class, result.getClass());
    }

    @Test
    public void testCharacterRepositoryCheckStatusShouldReturnCorrectly() throws Exception {
        String result = characterRepository.checkStatus();
        assertEquals("CONTINUE", result);
    }

    @Test
    public void testCharacterRepositoryGetHeroesShouldReturnCorrectly() throws Exception {
        int result = characterRepository.getHeroes().size();
        assertEquals(3, result);
    }

    @Test
    public void testCharacterRepositoryResetStatsAndEnemyShouldResetCorrectly() throws Exception {
        Chara dummy = characterRepository.getCharacter("Healer");
        dummy.attacked(200);

        characterRepository.resetStatsAndEnemy();
        assertEquals(150, dummy.getCurrentHealth());
        assertTrue(dummy.isAlive());
    }
}
