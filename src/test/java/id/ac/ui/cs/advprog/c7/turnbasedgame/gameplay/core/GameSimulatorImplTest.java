package id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.CharacterService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class GameSimulatorImplTest {

    @Mock
    CharacterService characterService = mock(CharacterService.class);

    GameSimulatorImpl gameSimulator;

    @BeforeEach
    public void setUp() throws Exception {
        this.gameSimulator = new GameSimulatorImpl(characterService);
    }

    @Test
    public void testGameSimulatorAttackCorrectlyImplemented() {
        String mockUser = "user";
        String mockTarget = "target";
        String mockStatus = "CONTINUE";
        when(characterService.checkStatus()).thenReturn(mockStatus);
        gameSimulator.attack(mockUser, mockTarget);
        verify(characterService, atLeastOnce()).attack(mockUser, mockTarget);
    }

    @Test
    public void testGameSimulatorUnableToAttackWhenGameStateIsNotPlayerTurn() {
        String mockUser = "user";
        String mockTarget = "target";
        String mockStatus = "WIN";
        when(characterService.checkStatus()).thenReturn(mockStatus);
        gameSimulator.attack(mockUser, mockTarget);
        gameSimulator.attack(mockUser, mockTarget);
        verify(characterService, atMostOnce()).attack(mockUser, mockTarget);
        gameSimulator.setState(GameState.PLAYER_TURN);
        mockStatus = "LOSE";
        when(characterService.checkStatus()).thenReturn(mockStatus);
        gameSimulator.attack(mockUser, mockTarget);
        gameSimulator.attack(mockUser, mockTarget);
        verify(characterService, atMost(2)).attack(mockUser, mockTarget);
    }

    @Test
    public void testGameSimulatorSkillCorrectlyImplemented() {
        String mockUser = "user";
        String mockStatus = "CONTINUE";
        when(characterService.checkStatus()).thenReturn(mockStatus);
        gameSimulator.skill(mockUser);
        verify(characterService, atLeastOnce()).skill(mockUser);
    }

    @Test
    public void testGameSimulatorUnableToSkillWhenGameStateIsNotPlayerTurn() {
        String mockUser = "user";
        String mockStatus = "WIN";
        when(characterService.checkStatus()).thenReturn(mockStatus);
        gameSimulator.skill(mockUser);
        gameSimulator.skill(mockUser);
        verify(characterService, atMostOnce()).skill(mockUser);
        gameSimulator.setState(GameState.PLAYER_TURN);
        mockStatus = "LOSE";
        when(characterService.checkStatus()).thenReturn(mockStatus);
        gameSimulator.skill(mockUser);
        gameSimulator.skill(mockUser);
        verify(characterService, atMost(2)).skill(mockUser);
    }

    @Test
    public void testGameSimulatorSetState() {
        String mockStatus1 = "CONTINUE";
        String mockStatus2 = "WIN";
        String mockStatus3 = "LOSE";
        gameSimulator.setState(GameState.PLAYER_TURN);
        when(characterService.checkStatus()).thenReturn(mockStatus1);
        assertEquals(gameSimulator.getState(), GameState.PLAYER_TURN);

        when(characterService.checkStatus()).thenReturn(mockStatus2);
        gameSimulator.setState(GameState.WIN);
        assertEquals(gameSimulator.getState(), GameState.WIN);

        when(characterService.checkStatus()).thenReturn(mockStatus3);
        gameSimulator.setState(GameState.LOSE);
        assertEquals(gameSimulator.getState(), GameState.LOSE);
    }

    @Test
    public void testGameSimulatorGetHeroesCorrectlyImplemented() {
        gameSimulator.getHeroes();
        verify(characterService, atLeastOnce()).getHeroes();
    }

    @Test
    public void testGameSimulatorGetEnemiesCorrectlyImplemented() {
        gameSimulator.getEnemies();
        verify(characterService, atLeastOnce()).getEnemies();
    }

    @Test
    public void testGameSimulatorLevelUpHeroesShouldCallCharacterServiceLevelUpHeroes() {
        gameSimulator.levelUpHeroes();
        verify(characterService, atLeastOnce()).levelUpHeroes();
    }

}
