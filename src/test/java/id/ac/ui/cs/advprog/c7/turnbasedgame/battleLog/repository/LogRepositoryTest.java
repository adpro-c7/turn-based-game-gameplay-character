package id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.repository;

import id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.core.Log;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
public class LogRepositoryTest {

    private LogRepository logRepository;

    @Mock
    private Map<String, Log> logMap;

    private Log log;

    @BeforeEach
    public void setUp() {
        logRepository = new LogRepository();
        logMap = new HashMap<>();
        log = new Log();
        logMap.put("token", log);
    }

    @Test
    public void whenLogRepositoryFindAllItShouldReturnLogMapList() {
        ReflectionTestUtils.setField(logRepository, "logMap", logMap);

        List<Log> acquiredLog = logRepository.findAll();

        assertThat(acquiredLog).isEqualTo(new ArrayList<>(logMap.values()));
    }

    @Test
    public void whenLogRepositoryFindByTokenItShouldReturnLog() {
        ReflectionTestUtils.setField(logRepository, "logMap", logMap);
        Log acquiredLog = logRepository.findByToken("token");

        assertThat(acquiredLog).isEqualTo(log);
    }

    @Test
    public void whenLogRepositorySaveItShouldSaveLog() {
        ReflectionTestUtils.setField(logRepository, "logMap", logMap);
        Log newLog = new Log();
        logRepository.save("token2", newLog);
        Log acquiredNewLog = logRepository.findByToken("token2");

        assertThat(acquiredNewLog).isEqualTo(newLog);
    }

    @Test
    public void whenPlayerLevelRepositoryDeleteByTokenItShouldReturnLevel() {
        ReflectionTestUtils.setField(logRepository, "logMap", logMap);
        logRepository.deleteByToken("token");
        Log acquiredLog = logRepository.findByToken("token");

        assertThat(acquiredLog).isEqualTo(null);
    }
}
