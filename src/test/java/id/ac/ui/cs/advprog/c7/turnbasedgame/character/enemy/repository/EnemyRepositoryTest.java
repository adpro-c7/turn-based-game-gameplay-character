package id.ac.ui.cs.advprog.c7.turnbasedgame.character.enemy.repository;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.enemy.core.Goblin;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class EnemyRepositoryTest {

    private EnemyRepository enemyRepository;

    @BeforeEach
    public void setUp() {
        enemyRepository = new EnemyRepository();
    }

    @Test
    public void testEnemyRepositoryOnRegisterEnemy() {
        enemyRepository.registerEnemy(new Goblin(100,0,20));
        Iterable<Chara> enemies = enemyRepository.getEnemies();

        assertThat(enemies).isNotEmpty();
    }

    @Test
    public  void testEnemyRepositoryOnBalanceEnemies() throws Exception{
        enemyRepository.balanceEnemies();
        int result = enemyRepository.getLevelEnemy();
        assertEquals(1, result);
    }

}