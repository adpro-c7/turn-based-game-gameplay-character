package id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.service;

import id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.core.Log;
import id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.repository.LogRepository;
import id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.service.LogServiceImpl;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;
import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core.GameFactory;
import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core.GameSimulator;
import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core.GameState;
import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.repository.GameRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class GameServiceTest {

    @Mock
    private GameRepository gameRepository;

    @Mock
    private GameSimulator gameSimulator;

    @Mock
    private GameFactory gameFactory;

    @Mock
    private LogServiceImpl logService;

    @Mock
    private LogRepository logRepository;

    @Mock
    private Log log;

    @InjectMocks
    private GameServiceImpl gameService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }


    @Test
    public void testGameServiceGetHeroesCorrectlyImplemented() {
        String token = "mockToken";
        when(gameRepository.getGameSimulatorByToken(token)).thenReturn(gameSimulator);
        gameService.getHeroes(token);
        verify(gameSimulator, atLeastOnce()).getHeroes();
    }

    @Test
    public void testGameServiceGetEnemiesCorrectlyImplemented() {
        String token = "mockToken";
        when(gameRepository.getGameSimulatorByToken(token)).thenReturn(gameSimulator);
        gameService.getEnemies(token);
        verify(gameSimulator, atLeastOnce()).getEnemies();
    }

    @Test
    public void testGameServiceAttackRelayed() {
        String token = "mockToken";
        String user = "mockUser";
        String target = "mockTarget";
        when(gameRepository.getGameSimulatorByToken(token)).thenReturn(gameSimulator);
        when(logService.findByToken(token)).thenReturn(log);
        List<String> mockResult = new ArrayList<>();
        when(gameSimulator.attack(user, target)).thenReturn(mockResult);
        assertEquals(mockResult, gameService.attack(token, user, target));
        verify(gameSimulator, atLeastOnce()).attack(user, target);
    }

    @Test
    public void testGameServiceSkillRelayed() {
        String token = "mockToken";
        String user = "mockUser";
        List<String> mockResult = new ArrayList<>();
        when(gameRepository.getGameSimulatorByToken(token)).thenReturn(gameSimulator);
        when(logService.findByToken(token)).thenReturn(log);
        when(gameSimulator.skill(user)).thenReturn(mockResult);
        assertEquals(mockResult, gameService.skill(token, user));
        verify(gameSimulator, atLeastOnce()).skill(user);
    }

    @Test
    public void testGameServiceGetStateRelayed() {
        String token = "mockToken";
        GameState mockState = GameState.PLAYER_TURN;
        when(gameRepository.getGameSimulatorByToken(token)).thenReturn(gameSimulator);
        when(gameSimulator.getState()).thenReturn(mockState);
        gameService.getState(token);
        verify(gameSimulator, atLeastOnce()).getState();
    }

    @Test
    public void testGameServiceDeleteGameSimulatorRelayed() {
        String token = "mockToken";
        gameService.deleteGameSimulator(token);
        verify(gameRepository, atLeastOnce()).deleteGameSimulatorByToken(token);
    }

    @Test
    public void testGameServiceCreateGameCorrectlyImplemented() {
        String token = "mockToken";
        when(gameRepository.getGameFactory()).thenReturn(gameFactory);
        when(gameFactory.createGame()).thenReturn(gameSimulator);
        gameService.createGameSimulator(token);
        verify(gameRepository, atLeastOnce()).registerGameSimulatorByToken(token, gameSimulator);
    }

    @Test
    public void testGameServiceLevelUpHeroesCorrectlyImplemented() {
        String token = "mockToken";
        String mockResponse = "mockResponse";
        when(gameRepository.getGameSimulatorByToken(token)).thenReturn(gameSimulator);
        when(logService.findByToken(token)).thenReturn(log);
        when(gameSimulator.levelUpHeroes()).thenReturn(mockResponse);
        gameService.levelUpHeroes(token);
        verify(gameSimulator, atLeastOnce()).levelUpHeroes();

        verify(log, atLeastOnce()).addLog(mockResponse);
    }
}
