package id.ac.ui.cs.advprog.c7.turnbasedgame.character;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.command.service.CommandService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;
import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class CharacterServiceTest {
    private Class<?> characterServiceClass;

    CharacterService characterService;

    @BeforeEach
    public void setup() throws Exception {
        characterServiceClass = Class.forName("id.ac.ui.cs.advprog.c7.turnbasedgame.character.CharacterService");
        characterService = new CharacterServiceImpl();
    }

    @Test
    public void testCharacterServiceGetHeroesShouldReturnCorrectly() throws Exception {
        int result = characterService.getHeroes().size();
        assertEquals(3, result);
    }

    @Test
    public void testCharacterServiceAttackShouldDecreaseHealth() throws Exception {
        Collection<Chara> heroes = characterService.getHeroes();
        Chara target = null;
        for(Chara chara: heroes) {
            if (chara.getName().equalsIgnoreCase("Knight")) {
                target = chara;
            }
        }

        characterService.attack("Healer", "Knight");
        int health = target.getCurrentHealth();
        assertEquals(280, health);
    }

    @Test
    public void testCharacterServiceHeroesSkillShouldBeImplementedCorrectly() throws Exception {
        Chara knight = characterService.getChara("Knight");
        Chara healer = characterService.getChara("Healer");
        Chara mage = characterService.getChara("Mage");

        characterService.skill("Knight");
        Iterable<Chara> enemies = characterService.getEnemies();
        for(Chara chara: enemies) {
            assertEquals(chara.getMaxHealth() - (knight.getAttack() * 3 / 5), chara.getCurrentHealth());
        }

        characterService.resetStatsAndEnemy();
        characterService.skill("Mage");
        enemies = characterService.getEnemies();
        for(Chara chara: enemies) {
            assertEquals(chara.getMaxHealth() - (mage.getAttack() / 2), chara.getCurrentHealth());
        }

        characterService.enemyAttack();
        Iterable<Chara> heroes = characterService.getHeroes();
        int totalHealthBefore = 0;
        for(Chara chara: heroes) {
            totalHealthBefore += chara.getCurrentHealth();
        }

        characterService.skill("Healer");
        heroes = characterService.getHeroes();
        int totalHealthNow = 0;
        for(Chara chara: heroes) {
            totalHealthNow += chara.getCurrentHealth();
        }
        assertTrue(totalHealthNow > totalHealthBefore);
    }

    @Test
    public void testResetStatsAndEnemyShouldReset() throws Exception {
        Collection<Chara> heroes = characterService.getHeroes();
        Iterable<Chara> enemies = characterService.getEnemies();
        Chara target = null;
        for(Chara chara: heroes) {
            if (chara.getName().equalsIgnoreCase("knight")) {
                target = chara;
            }
        }

        characterService.attack("Healer", "Knight");
        characterService.resetStatsAndEnemy();

        Iterable<Chara> newEnemies = characterService.getEnemies();
        int health = target.getCurrentHealth();

        assertEquals(target.getMaxHealth(), health);
        assertNotEquals(enemies, newEnemies);

        for(Chara enemy: enemies) {
            assertEquals(enemy.getMaxHealth(), enemy.getCurrentHealth());
        }
    }

    @Test
    public void testCharacterServiceCheckStatusShouldReturnCorrectly() throws Exception {
        String result = characterService.checkStatus();
        assertEquals("CONTINUE", result);

        for(int i = 0; i < 50; i++) {
            characterService.attack("Healer", "Goblin");
            characterService.attack("Knight", "Ogre");
            characterService.attack("Mage", "Troll");
        }
        result = characterService.checkStatus();
        assertEquals("WIN", result);

        characterService.resetStatsAndEnemy();
        for(int i = 0; i < 50; i++) {
            characterService.attack("Goblin", "Healer");
            characterService.attack("Ogre", "Knight");
            characterService.attack("Troll", "Mage");
        }
        result = characterService.checkStatus();
        assertEquals("LOSE", result);
    }

    @Test
    public void testEnemyAttackShouldDecreaseRandomHeroHealth() throws Exception {
        Collection<Chara> heroes = characterService.getHeroes();
        boolean attacked = false;
        characterService.enemyAttack();

        for(Chara hero: heroes) {
            if(hero.getCurrentHealth() < hero.getMaxHealth()){
                attacked = true;
            }
        }

        assertTrue(attacked);
    }

    @Test
    public void testCharacterServiceLevelUpHeroesShouldIncreaseStats() throws Exception {
        int maxHealthKnight = characterService.getChara("Knight").getMaxHealth();
        int maxHealthHealer = characterService.getChara("Healer").getMaxHealth();
        int maxHealthMage = characterService.getChara("Mage").getMaxHealth();
        
        characterService.levelUpHeroes();

        assertTrue(maxHealthKnight < characterService.getChara("Knight").getMaxHealth());
        assertTrue(maxHealthHealer < characterService.getChara("Healer").getMaxHealth());
        assertTrue(maxHealthMage < characterService.getChara("Mage").getMaxHealth());
    }

}
