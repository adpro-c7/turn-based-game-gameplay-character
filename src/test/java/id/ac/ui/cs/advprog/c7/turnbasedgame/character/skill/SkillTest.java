package id.ac.ui.cs.advprog.c7.turnbasedgame.character.skill;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class SkillTest {

    private Class<?> explosionSkill;
    private Class<?> chargeAttackSkill;
    private Class<?> haveNoSkill;
    private Class<?> healSkill;

    @BeforeEach
    public void setUp() throws Exception {
        explosionSkill = Class.forName("id.ac.ui.cs.advprog.c7.turnbasedgame.character.skill.ExplosionSkill");
        chargeAttackSkill = Class.forName("id.ac.ui.cs.advprog.c7.turnbasedgame.character.skill.ChargeAttackSkill");
        haveNoSkill = Class.forName("id.ac.ui.cs.advprog.c7.turnbasedgame.character.skill.HaveNoSkill");
        healSkill = Class.forName("id.ac.ui.cs.advprog.c7.turnbasedgame.character.skill.HealSkill");
    }

    @Test
    public void testExplosionSkillOverrideUseSkillMethod() throws Exception {
        Method useSkill = explosionSkill.getDeclaredMethod("useSkill");

        assertTrue(Modifier.isPublic(useSkill.getModifiers()));
        assertEquals("java.lang.String", useSkill.getGenericReturnType().getTypeName());
    }

    @Test
    public void testChargeAttackSkillOverrideUseSkillMethod() throws Exception {
        Method useSkill = chargeAttackSkill.getDeclaredMethod("useSkill");

        assertTrue(Modifier.isPublic(useSkill.getModifiers()));
        assertEquals("java.lang.String", useSkill.getGenericReturnType().getTypeName());
    }

    @Test
    public void testHaveNoSkillOverrideUseSkillMethod() throws Exception {
        Method useSkill = haveNoSkill.getDeclaredMethod("useSkill");

        assertTrue(Modifier.isPublic(useSkill.getModifiers()));
        assertEquals("java.lang.String", useSkill.getGenericReturnType().getTypeName());
    }

    @Test
    public void testHealSkillOverrideUseSkillMethod() throws Exception {
        Method useSkill = healSkill.getDeclaredMethod("useSkill");

        assertTrue(Modifier.isPublic(useSkill.getModifiers()));
        assertEquals("java.lang.String", useSkill.getGenericReturnType().getTypeName());
    }

    @Test
    public void testHaveNoSkillShouldReturnCorrectly() throws Exception {
        Skill haveNoSkill = new HaveNoSkill();
        String res = haveNoSkill.useSkill();
        assertEquals("Do nothing", res);
    }
}
