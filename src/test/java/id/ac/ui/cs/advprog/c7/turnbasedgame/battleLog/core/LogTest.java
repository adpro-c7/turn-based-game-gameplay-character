package id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class LogTest {

    private Class<?> logClass;

    @InjectMocks
    private Log log;

    @BeforeEach
    public void setUp() throws Exception {
        logClass = Class.forName("id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.core.Log");
        log = new Log();
        log.addLog("testLog");
    }

    @Test
    public void testLogIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(logClass.getModifiers()));
    }

    @Test
    public void testAddLogMethodReturnCorrectAnswer() throws Exception{
        log.addLog("testLog");
        assertTrue(log.getCurrentLog().equals(Arrays.asList("testLog", "testLog")));
    }

    @Test
    public void testAddLogsMethodReturnCorrectAnswer() throws Exception{
        log.addLogs(Arrays.asList("testLog"));
        assertTrue(log.getCurrentLog().equals(Arrays.asList("testLog", "testLog")));
    }

    @Test
    public void testResetMethodReturnCorrectAnswer() throws Exception{
        log.reset();
        assertEquals(Arrays.asList(), log.getCurrentLog());
    }

    @Test
    public void testGetCurrentLogReturnCorrectAnswer() throws Exception{
        assertEquals(Arrays.asList("testLog"), log.getCurrentLog());
    }
}